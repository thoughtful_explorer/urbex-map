#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import re
import simplekml

#Set filename/path for KML file output
kmlfile = "urbex.kml"
#Set page URL
pageURL = "https://www.urbex.nl/locations"

#Returns reusable BeautifulSoup of the site, defaulting to the locations list page
def getsoupinit(url=pageURL):
    #Start a session with the given page URL
    session = requests.Session()
    page = session.get(url)
    #Return soupified HTML
    return BeautifulSoup(page.content, 'html.parser')

#Returns a list of all sites
def getsites():
    #Initialize the soup as a list of all ul's of the lcp_catlist class
    soup = getsoupinit()(class_="lcp_catlist")
    #Initialize a list to hold links
    links =[]
    #Loop through each ul...
    for ul in soup:
        #...and within each ul, through each li...
        for li in ul:
            #...and append each of those links within the list items to the links list
            links.append(li.a["href"])
    return links

#Returns site information for a given url
def getsite(url):
    soup = getsoupinit(url)
    #Get the name and add Urbex prefix to name to reduce vagueness if presented among other map layers
    name = "Urbex - " + str(soup(class_="postitle")[0].get_text())
    #Get the coordinates
    coords = re.split(",",soup.find_all(attrs={"name":"ICBM"})[0]["content"])
    lat = coords[0]
    lng = coords[1]
    #Get the status of the site
    status = soup.find_all("span", class_="uagb-icon-list__label")[2].get_text()
    #Tie it all together with a dictionary of attributes we're interested in for each site
    sitedict = {"name":name,"status":status,"lat":lat,"lng":lng}
    return sitedict

#Returns a list of all the sites
def getallsites():
    #Initialize sites list
    sites=[]
    #Loop through all the sites
    for site in getsites():
        #Use try to avoid erroring out on empty geo values, and add the site to the site list
        try:
            sites.append(getsite(site))
	#...but if there is missing data, skip it because there are some pages which lack geographic information
        except IndexError:
            continue
    #Python gets unhappy about tab spacing if we try to do all of this in the above loop, so loop again after sites with coordinates are listed
    for site in sites:
        #Eliminate any sites that have been demolished or burned down - no interest there
        if re.search("^Demolished.*",site.get("status")) or re.search("^Burned down.*",site.get("status")):
            sites.remove(site)
    return sites

#Saves a KML file of a given list of stores
def createkml(sites):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through datamap for each site
    for site in sites:
        #Get the name of the site
        sitename = site["name"]
        #Get coordinates of the site
        lat = site["lat"]
        lng = site["lng"]
        #First, create the point name and description in the kml
        point = kml.newpoint(name=sitename,description="urbex")
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)
#Bring it all together
createkml(getallsites())
