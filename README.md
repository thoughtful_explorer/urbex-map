# Urbex Location Extractor
Extracts Urbex (Dutch urban exploration site) locations that still exist from the official website, and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * Regular expressions module for matching text values
    * Simplekml for easily building KML files
* Also of course depends on official [Urbex](https://www.urbex.nl/locations/) website.
